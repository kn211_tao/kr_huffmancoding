﻿
namespace KR_HuffmanCode
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button_code = new System.Windows.Forms.Button();
            this.label_valueCount = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.rtb_codeBefore = new System.Windows.Forms.RichTextBox();
            this.rtb_textBefore = new System.Windows.Forms.RichTextBox();
            this.label_differenceValue = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label_afterValue = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label_beforeValue = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.rtb_codeAfter = new System.Windows.Forms.RichTextBox();
            this.rtb_textAfter = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.treeView_symbolsCode = new System.Windows.Forms.TreeView();
            this.label8 = new System.Windows.Forms.Label();
            this.label_timerValue = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button_code
            // 
            this.button_code.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(164)))), ((int)(((byte)(233)))), ((int)(((byte)(158)))));
            this.button_code.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button_code.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button_code.ForeColor = System.Drawing.Color.DarkGreen;
            this.button_code.Location = new System.Drawing.Point(163, 412);
            this.button_code.Name = "button_code";
            this.button_code.Size = new System.Drawing.Size(301, 53);
            this.button_code.TabIndex = 53;
            this.button_code.Text = "Шифрування";
            this.button_code.UseVisualStyleBackColor = false;
            this.button_code.Click += new System.EventHandler(this.button_code_Click);
            // 
            // label_valueCount
            // 
            this.label_valueCount.AutoSize = true;
            this.label_valueCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label_valueCount.ForeColor = System.Drawing.Color.DarkGreen;
            this.label_valueCount.Location = new System.Drawing.Point(697, 441);
            this.label_valueCount.Name = "label_valueCount";
            this.label_valueCount.Size = new System.Drawing.Size(0, 20);
            this.label_valueCount.TabIndex = 52;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label5.ForeColor = System.Drawing.Color.DarkGreen;
            this.label5.Location = new System.Drawing.Point(478, 442);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(224, 20);
            this.label5.TabIndex = 51;
            this.label5.Text = "Кількість різних символів";
            // 
            // rtb_codeBefore
            // 
            this.rtb_codeBefore.BackColor = System.Drawing.SystemColors.Window;
            this.rtb_codeBefore.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.rtb_codeBefore.Location = new System.Drawing.Point(480, 34);
            this.rtb_codeBefore.Name = "rtb_codeBefore";
            this.rtb_codeBefore.ReadOnly = true;
            this.rtb_codeBefore.Size = new System.Drawing.Size(301, 164);
            this.rtb_codeBefore.TabIndex = 50;
            this.rtb_codeBefore.Text = "";
            // 
            // rtb_textBefore
            // 
            this.rtb_textBefore.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.rtb_textBefore.Location = new System.Drawing.Point(163, 34);
            this.rtb_textBefore.Name = "rtb_textBefore";
            this.rtb_textBefore.Size = new System.Drawing.Size(301, 164);
            this.rtb_textBefore.TabIndex = 49;
            this.rtb_textBefore.Text = "";
            // 
            // label_differenceValue
            // 
            this.label_differenceValue.AutoSize = true;
            this.label_differenceValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label_differenceValue.ForeColor = System.Drawing.Color.DarkGreen;
            this.label_differenceValue.Location = new System.Drawing.Point(738, 415);
            this.label_differenceValue.Name = "label_differenceValue";
            this.label_differenceValue.Size = new System.Drawing.Size(0, 20);
            this.label_differenceValue.TabIndex = 48;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label7.ForeColor = System.Drawing.Color.DarkGreen;
            this.label7.Location = new System.Drawing.Point(478, 416);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(268, 20);
            this.label7.TabIndex = 47;
            this.label7.Text = "Довжина коду зменшилась на ";
            // 
            // label_afterValue
            // 
            this.label_afterValue.AutoSize = true;
            this.label_afterValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label_afterValue.ForeColor = System.Drawing.Color.DarkGreen;
            this.label_afterValue.Location = new System.Drawing.Point(646, 215);
            this.label_afterValue.Name = "label_afterValue";
            this.label_afterValue.Size = new System.Drawing.Size(0, 20);
            this.label_afterValue.TabIndex = 46;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label6.ForeColor = System.Drawing.Color.DarkGreen;
            this.label6.Location = new System.Drawing.Point(480, 216);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(166, 20);
            this.label6.TabIndex = 45;
            this.label6.Text = "Його двоїчний код";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label4.ForeColor = System.Drawing.Color.DarkGreen;
            this.label4.Location = new System.Drawing.Point(162, 217);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(219, 20);
            this.label4.TabIndex = 44;
            this.label4.Text = "Текст після шифрування";
            // 
            // label_beforeValue
            // 
            this.label_beforeValue.AutoSize = true;
            this.label_beforeValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label_beforeValue.ForeColor = System.Drawing.Color.DarkGreen;
            this.label_beforeValue.Location = new System.Drawing.Point(645, 10);
            this.label_beforeValue.Name = "label_beforeValue";
            this.label_beforeValue.Size = new System.Drawing.Size(0, 20);
            this.label_beforeValue.TabIndex = 43;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label3.ForeColor = System.Drawing.Color.DarkGreen;
            this.label3.Location = new System.Drawing.Point(479, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(166, 20);
            this.label3.TabIndex = 42;
            this.label3.Text = "Його двоїчний код";
            // 
            // rtb_codeAfter
            // 
            this.rtb_codeAfter.BackColor = System.Drawing.SystemColors.Window;
            this.rtb_codeAfter.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.rtb_codeAfter.Location = new System.Drawing.Point(480, 242);
            this.rtb_codeAfter.Name = "rtb_codeAfter";
            this.rtb_codeAfter.ReadOnly = true;
            this.rtb_codeAfter.Size = new System.Drawing.Size(301, 164);
            this.rtb_codeAfter.TabIndex = 41;
            this.rtb_codeAfter.Text = "";
            // 
            // rtb_textAfter
            // 
            this.rtb_textAfter.BackColor = System.Drawing.SystemColors.Window;
            this.rtb_textAfter.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.rtb_textAfter.Location = new System.Drawing.Point(163, 242);
            this.rtb_textAfter.Name = "rtb_textAfter";
            this.rtb_textAfter.ReadOnly = true;
            this.rtb_textAfter.Size = new System.Drawing.Size(301, 164);
            this.rtb_textAfter.TabIndex = 40;
            this.rtb_textAfter.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.ForeColor = System.Drawing.Color.DarkGreen;
            this.label2.Location = new System.Drawing.Point(163, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(197, 20);
            this.label2.TabIndex = 39;
            this.label2.Text = "Текст до шифрування";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.Color.DarkGreen;
            this.label1.Location = new System.Drawing.Point(15, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 20);
            this.label1.TabIndex = 38;
            this.label1.Text = "Коди символів";
            // 
            // treeView_symbolsCode
            // 
            this.treeView_symbolsCode.Location = new System.Drawing.Point(15, 33);
            this.treeView_symbolsCode.Name = "treeView_symbolsCode";
            this.treeView_symbolsCode.Size = new System.Drawing.Size(133, 373);
            this.treeView_symbolsCode.TabIndex = 37;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label8.ForeColor = System.Drawing.Color.DarkGreen;
            this.label8.Location = new System.Drawing.Point(13, 421);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 15);
            this.label8.TabIndex = 54;
            this.label8.Text = "Час :";
            // 
            // label_timerValue
            // 
            this.label_timerValue.AutoSize = true;
            this.label_timerValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label_timerValue.ForeColor = System.Drawing.Color.DarkGreen;
            this.label_timerValue.Location = new System.Drawing.Point(14, 445);
            this.label_timerValue.Name = "label_timerValue";
            this.label_timerValue.Size = new System.Drawing.Size(0, 15);
            this.label_timerValue.TabIndex = 55;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(253)))), ((int)(((byte)(208)))));
            this.ClientSize = new System.Drawing.Size(800, 477);
            this.Controls.Add(this.label_timerValue);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.button_code);
            this.Controls.Add(this.label_valueCount);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.rtb_codeBefore);
            this.Controls.Add(this.rtb_textBefore);
            this.Controls.Add(this.label_differenceValue);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label_afterValue);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label_beforeValue);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.rtb_codeAfter);
            this.Controls.Add(this.rtb_textAfter);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.treeView_symbolsCode);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(816, 516);
            this.MinimumSize = new System.Drawing.Size(816, 516);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Алгоритм Хафмана. Тихоліз А.О. КН-21-1(2)";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_code;
        private System.Windows.Forms.Label label_valueCount;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox rtb_codeBefore;
        private System.Windows.Forms.RichTextBox rtb_textBefore;
        private System.Windows.Forms.Label label_differenceValue;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label_afterValue;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label_beforeValue;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox rtb_codeAfter;
        private System.Windows.Forms.RichTextBox rtb_textAfter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TreeView treeView_symbolsCode;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label_timerValue;
    }
}

