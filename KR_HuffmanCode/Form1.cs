﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics; 

namespace KR_HuffmanCode
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public struct huffmannTree /// структура для вузлів дерева
        {
            public String text; // текст
            public String code; // двоїчний код тексту
            public int frequency; // частота символів

            public huffmannTree(String text, String code, int frequency) /// конструктор
            {
                this.text = text;
                this.code = code;
                this.frequency = frequency;
            }
        };

        private void button_code_Click(object sender, EventArgs e)
        {
            Dictionary<char, int> symbolFreq = new Dictionary<char, int>(); // для таюлиці частоти зустрчі окремих символів 

            List<huffmannTree> source = new List<huffmannTree>(); // дане дерево для опрацювання
            List<huffmannTree> tree = new List<huffmannTree>(); // додаткове дерево
            List<huffmannTree> resTree = new List<huffmannTree>(); // дерево з результатом


            void sortTree() // метод сортування дерева по спаданню
            { // після сортування два останніх вузла - це ті, що зустрічаються найрідше
                for (int i = 0; i < tree.Count - 1; i++)
                {
                    for (int j = i; j < tree.Count; j++)
                    {
                        if (tree[i].frequency < tree[j].frequency)
                        {
                            huffmannTree tmp = tree[i];
                            tree[i] = tree[j];
                            tree[j] = tmp;
                        }
                    }
                }
            }

            // отримуємо текст
            String text = rtb_textBefore.Text;

            // очищаємо попереднє шифрування
            treeView_symbolsCode.Nodes.Clear();
            rtb_textAfter.Text = "";
            rtb_codeBefore.Text = "";
            rtb_codeAfter.Text = "";

            for (int i = 0; i < rtb_textBefore.Text.Length; i++)
            {// вираховуємо частоту символів
                if (symbolFreq.Keys.Contains(text[i]))
                {// якщо символ вже є в словнику
                    symbolFreq[text[i]]++; // збільшуємо його частоту
                }
                else // якщо символ зустрічається вперше
                {// добавляємо символ до словника
                    symbolFreq.Add(text[i], 1);
                }
            }

            // заповнюємо дерева для подальшої роботи з ними
            foreach (KeyValuePair<char, int> Pair in symbolFreq)
            {
                source.Add(new huffmannTree(Pair.Key.ToString(), " ", Pair.Value));
                tree.Add(new huffmannTree(Pair.Key.ToString(), " ", Pair.Value));
                resTree.Add(new huffmannTree(Pair.Key.ToString(), " ", Pair.Value));
            }

            String codeBefore = ""; /// для зберігання бітового коду
            BitArray bit = new BitArray(Encoding.ASCII.GetBytes(text));

            for (int i = 0; i < bit.Count; i++)
            { // переводимо в біти
                if (bit[i])
                    codeBefore += "1";
                else
                    codeBefore += "0";
            }

            Stopwatch timer = new Stopwatch(); // creating new instance of the stopwatch
            timer.Start(); // to start the timer in code
            while (tree.Count > 1)
            { // будуємо дерево

                sortTree(); // сортуємо дерево по спаданню

                for (int i = 0; i < source.Count; i++)
                { 
                    if (tree[tree.Count - 2].text.Contains(source[i].text))
                    { // приписуємо нуль до першого дочірного вузла
                        resTree[i] = new huffmannTree(resTree[i].text, "0" + resTree[i].code, resTree[i].frequency);
                    }
                    else if (tree[tree.Count - 1].text.Contains(source[i].text))
                    { // іншому 1
                        resTree[i] = new huffmannTree(resTree[i].text, "1" + resTree[i].code, resTree[i].frequency);
                    }
                }

                // об'єднуємо два останніх вузла
                tree[tree.Count - 2] = new huffmannTree(tree[tree.Count - 2].text + tree[tree.Count - 1].text, "",
                                       tree[tree.Count - 2].frequency + tree[tree.Count - 1].frequency);
                tree.RemoveAt(tree.Count - 1); // видаляємо найменший вузол
            }
            timer.Stop();
            label_timerValue.Text = timer.Elapsed.ToString() +" мс";
            

            for (int i = 0; i < source.Count; i++)
            { // виводимо коди символів    
                treeView_symbolsCode.Nodes.Add(resTree[i].text + " --> " + resTree[i].code + ";");
            }

            
            rtb_codeBefore.Text = codeBefore; // виводимо бітовий код до шифрування 
            for (int i = 0; i < text.Length; i++)
            {// виводимо бітовий код після шифрування
                foreach (huffmannTree node in resTree)
                {
                    if (node.text == text[i].ToString())
                    {
                        rtb_codeAfter.Text += node.code;
                    }
                }
            }

            label_beforeValue.Text = bit.Count.ToString(); // виводимо кількість символів до шифрування 
            label_afterValue.Text = rtb_codeAfter.Text.Length.ToString(); // виводимо кількість символів після шифрування

            label_valueCount.Text = symbolFreq.Count.ToString();// виводимо кількість різних символів
            

            // кодуємо назад в букви
            String encodedText = rtb_codeAfter.Text;
            for (int i = 0; i < encodedText.Length - 7; i += 8)
            {
                int code = 0;
                for (int ind = 0; ind < 8; ind++)
                {// групуємо по вісім біт
                    if (encodedText[i + ind] == '1')
                    { // конвертуємо в число
                        code += Convert.ToInt32(Math.Pow(2, 7 - ind));
                    }
                }
                rtb_textAfter.Text += (char)code; // виводимо символ за кодом
            }

            // на скільки відсотків код став коротший
            int beforeValue = rtb_codeBefore.Text.Length;
            int aferValue = rtb_codeAfter.Text.Length;       
            if (beforeValue > 0)
            {
                label_differenceValue.Text = "";
                int difference = 100 - (aferValue * 100 / beforeValue);
                label_differenceValue.Text += difference + "%";
            }
        }
    }
}